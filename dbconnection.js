var mysql = require('mysql');

var defaultConnection = process.env.CLEARDB_DATABASE_URL || process.env.JAWSDB_URL || DB_URL ||'mysql://root:password@127.0.0.1/igiftlife';
defaultConnection += '?multipleStatements=true&connectionLimit-5'

var con = mysql.createPool(defaultConnection);

module.exports = con;
