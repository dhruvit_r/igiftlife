import { Component } from '@angular/core';

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
})
export class AwardsComponent {
  images = [
    {
      src: 'assets/about-us/award1.jpg',
    },
    {
      src: 'assets/about-us/award2.jpg',
    },
  ];
}
