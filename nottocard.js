var fs = require('fs');
const path = require('path');
const { createCanvas, loadImage } = require('canvas');
const canvas = createCanvas(1020, 500)
const ctx = canvas.getContext('2d')
PDFDocument = require('pdfkit')
var email = require("emailjs");

/* For localhost */
var DIR_dc = path.join(__dirname, '/src/assets/uploads/donor-card-pdf/');

/* For server */
//var DIR_dc = path.join(__dirname, '/dist/assets/uploads/donor-card-pdf/');

//To check if a directory exists, if not create that directory
function ensureExists(path, mask, cb) {
  if (typeof mask == 'function') { // allow the `mask` parameter to be optional
    cb = mask;
    mask = 0777;
  }
  fs.mkdir(path, mask, function (err) {
    if (err) {
      if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
      else cb(err); // something else went wrong
    } else cb(null); // successfully created folder
  });
}

loadImage("nottocard1.jpg").then((image) => {
  ctx.drawImage(image, 0, 0, 500, 300)
  ctx.font = '20px Arial';
  ctx.fillText('Abc Xyz',103, 90);  //specify the name of donor here

  ensureExists(DIR_dc, 0777, function (err) {

    if (err) throw err;// handle folder creation error
    else {
    }
  });
                                 //Id Card no. 
  fs.writeFile(path.join(DIR_dc, '982828' + '.jpg'), canvas.toBuffer(), (err) => {
    if (err) throw err;
  });
  //console.log('<img src="' + canvas.toDataURL() + '" />')
})

loadImage("nottocard2.jpg").then((image) => {
  ctx.drawImage(image, 520, 0, 500, 300)

  ctx.fillText('298826', 795, 37);   //specify registration no. here
  // ctx.beginPath();
  // ctx.moveTo(590, 135);
  // ctx.lineTo(600, 145);
  // ctx.stroke();
                                 //Id Card no. 
  fs.writeFile(path.join(DIR_dc, '982828' + '.jpg'), canvas.toBuffer(), (err) => {
    if (err) throw err;

    //Create a document
    doc = new PDFDocument
                                                    //Id Card no.
    doc.pipe(fs.createWriteStream(path.join(DIR_dc, '982828' + '.pdf')));
                                //Id Card no.
    doc.image(path.join(DIR_dc, '982828' + '.jpg'), 0, 0, { width: 520, height: 280 });
    doc.end()
                                //Id Card no.
    fs.unlink(path.join(DIR_dc, '982828' + '.jpg'), function (err) {
      if (err) throw err;
      // if no error, file has been deleted successfully
    });

    var server = email.server.connect({
      user: "igiftlifefoundation@gmail.com",   //email id from which mail has to be sent
      password: "igiftlife@1234",    //password of that email id 
      host: "smtp.gmail.com",
      port: 465,
      ssl: true,
      timeout: 5000
    });

    var message = {
      from: "IGiftLife <igiftlifefoundation@gmail.com>",
                //email id of donor
      to: "<" + 'abc@gmail.com' + ">",
      subject: "IGiftLife Pledge Donor Card",
      attachment:
        [
          { data: "<html>Your pledge for organ donation has been submitted successfully. Your donor card is attached with this mail, you can take a printout of it and keep it with you.</html>", alternative: true },
                                    //Id Card no.
          { path: path.join(DIR_dc, '982828' + '.pdf'), type: "application/pdf", name: "Donor-Card.pdf" }
        ]
    };

    // send the message and get a callback with an error or details of the message that was sent
    server.send(message, function (err, message) { });
  });
})