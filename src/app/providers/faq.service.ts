import { DomSanitizer } from '@angular/platform-browser';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqService {
  private urls = [
    'assets/faqs/deceased-donor-related-transplant.json',
    'assets/faqs/legal-ethical-issues.json',
    'assets/faqs/living-donor-related-transplant.json',
    'assets/faqs/organ-tissues-transplant.json',
    'assets/faqs/organs-tissues-donation.json',
    'assets/faqs/facts.json'
  ];

  constructor(public http: HttpClient, public sanitizer: DomSanitizer) {}

  getAll(): Observable<any[]> {
    return forkJoin(this.urls.map(url => this.http.get(url)));
  }
}
