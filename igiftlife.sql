-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2018 at 06:31 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `igiftlife`
--

-- --------------------------------------------------------

--
-- Table structure for table `donor_card`
--

CREATE TABLE `donor_card` (
  `full_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `blood_group` varchar(5) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `emergency_contact` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `pin_code` varchar(6) NOT NULL,
  `id_card_type` varchar(20) NOT NULL,
  `id_card_number` varchar(30) NOT NULL,
  `id_card_image_path` varchar(300) NOT NULL,
  `registration_date` date NOT NULL,
  `is_notto` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donor_card`
--

INSERT INTO `donor_card` (`full_name`, `father_name`, `gender`, `dob`, `blood_group`, `phone_number`, `emergency_contact`, `email`, `address`, `city`, `state`, `pin_code`, `id_card_type`, `id_card_number`, `id_card_image_path`, `registration_date`, `is_notto`) VALUES
('nb', 'g', 'Male', '2018-11-11', 'A+', '+91 999', '+91 888', 'hh@a.a', 'nb', 'b', 'b', 'b', 'AadharCard', '8778', 'D:\\Projects\\igiftlife\\src\\assets\\uploads\\id-card-photo\\8778.png', '2018-12-11', '0'),
('Ksjs', 'bxb', 'Male', '2018-11-11', 'AB+', '+91 82828-27272', '+91 37737-26262', 'bc@c.c', 'bc', 'h', 'dy', '83722', 'AadharCard', '883377', 'D:\\Projects\\igiftlife\\src\\assets\\uploads\\id-card-photo\\883377.jpg', '2018-12-11', '0'),
('Anushka Nagar', 'h', 'Male', '2018-11-11', 'A+', '+91 99922-27727', '+91 28287-66663', 'anushka.nagar@sitpune.edu.in', 'bah', 'shh', 'hdh', '8873', 'AadharCard', '92287663', 'D:\\Projects\\igiftlife\\src\\assets\\uploads\\id-card-photo\\92287663.jpg', '2018-12-11', '0'),
('bb', 'g', 'Male', '2018-06-18', 'A+', '+91 93883-83838', '+91 83883-33333', 'anushka.nagar@sitpune.edu.in', 'bb', 'yy', 'ett', '828828', 'AadharCard', '9383838', 'D:\\Projects\\igiftlife\\src\\assets\\uploads\\id-card-photo\\9383838.png', '2018-12-11', '0'),
('bvg', 'gf', 'Male', '2018-11-13', 'AB+', '+91 73773-73888', '+91 66666-66666', 'anushka.nagar@sitpune.edu.in', 'bb', 'b', 'v', '8999', 'AadharCard', '983838', 'D:\\Projects\\igiftlife\\src\\assets\\uploads\\id-card-photo\\983838.jpg', '2018-12-11', '0');

-- --------------------------------------------------------

--
-- Table structure for table `organs`
--

CREATE TABLE `organs` (
  `id` int(11) NOT NULL,
  `id_card_number` varchar(30) NOT NULL,
  `heart` varchar(6) NOT NULL,
  `lungs` varchar(6) NOT NULL,
  `kidneys` varchar(6) NOT NULL,
  `liver` varchar(6) NOT NULL,
  `pancreas` varchar(6) NOT NULL,
  `intestine` varchar(6) NOT NULL,
  `all_organs` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organs`
--

INSERT INTO `organs` (`id`, `id_card_number`, `heart`, `lungs`, `kidneys`, `liver`, `pancreas`, `intestine`, `all_organs`) VALUES
(1, '8778', '1', '1', '1', '0', '0', '0', '0'),
(2, '92287663', '0', '0', '0', '0', '0', '0', '1'),
(3, '883377', '0', '0', '0', '1', '0', '1', '0'),
(4, '9383838', '0', '1', '1', '1', '0', '0', '0'),
(5, '983838', '0', '0', '1', '0', '0', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tissues`
--

CREATE TABLE `tissues` (
  `id` int(11) NOT NULL,
  `id_card_number` varchar(30) NOT NULL,
  `corneas` varchar(6) NOT NULL,
  `skin` varchar(6) NOT NULL,
  `bones` varchar(6) NOT NULL,
  `heart_valves` varchar(6) NOT NULL,
  `blood_vessels` varchar(6) NOT NULL,
  `all_tissues` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tissues`
--

INSERT INTO `tissues` (`id`, `id_card_number`, `corneas`, `skin`, `bones`, `heart_valves`, `blood_vessels`, `all_tissues`) VALUES
(1, '8778', '0', '1', '0', '0', '0', '0'),
(2, '92287663', '0', '0', '0', '0', '0', '1'),
(3, '883377', '0', '0', '0', '1', '1', '0'),
(4, '9383838', '0', '0', '0', '1', '0', '0'),
(5, '983838', '0', '1', '0', '1', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `witness`
--

CREATE TABLE `witness` (
  `witness_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `id_card_number` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `witness`
--

INSERT INTO `witness` (`witness_id`, `full_name`, `relationship`, `phone_number`, `email`, `address`, `id_card_number`) VALUES
(1, 'bv', 'h', '+91 778', 'hh@q.q', 'gg', '8778'),
(2, 'bb', 'b', '+91 77', 'vw@w.w', 'nb', '8778'),
(3, 'bv', 'dgg', '+91 83337-73733', 'hdb@d.d', 'bd', '92287663'),
(4, 'h', 'gg', '+91 63788-28228', 'dhd@d.d', 'd', '92287663'),
(5, 'bv', 'gf', '+91 88772-72727', 'bd@d.d', 'b', '883377'),
(6, 'vv', 'r', '+91 99838-83833', 'cc@c.c', 'c', '883377'),
(7, 'vv', 'db', '+91 33773-73777', 'c@c.c', 'cc', '9383838'),
(8, 'yyt', 'dvv', '+91 89393-93939', 'nc@c.c', 'bb', '9383838'),
(9, 'vv', 't', '+91 38383-88888', 'bd@d.d', 'v', '983838'),
(10, 'v', 'g', '+91 66777-77777', 'f@s.s', 'gg', '983838');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donor_card`
--
ALTER TABLE `donor_card`
  ADD PRIMARY KEY (`id_card_number`);

--
-- Indexes for table `organs`
--
ALTER TABLE `organs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card_number` (`id_card_number`);

--
-- Indexes for table `tissues`
--
ALTER TABLE `tissues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card_number` (`id_card_number`);

--
-- Indexes for table `witness`
--
ALTER TABLE `witness`
  ADD PRIMARY KEY (`witness_id`),
  ADD KEY `id_card_number` (`id_card_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organs`
--
ALTER TABLE `organs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tissues`
--
ALTER TABLE `tissues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `witness`
--
ALTER TABLE `witness`
  MODIFY `witness_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organs`
--
ALTER TABLE `organs`
  ADD CONSTRAINT `id_card_number` FOREIGN KEY (`id_card_number`) REFERENCES `donor_card` (`id_card_number`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `witness`
--
ALTER TABLE `witness`
  ADD CONSTRAINT `witness_ibfk_1` FOREIGN KEY (`id_card_number`) REFERENCES `donor_card` (`id_card_number`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
