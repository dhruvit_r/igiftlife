import { Component } from '@angular/core';

@Component({
  selector: 'app-founders-section',
  templateUrl: './founders-section.component.html',
  styleUrls: ['./founders-section.component.scss']
})
export class FoundersSectionComponent {
  cards = [
    {
      title: 'Mr. Tejpal Singh Batra',
      subtitle: 'Co-founder and President',
      imgSrc: 'assets/founders/tejpal sir.png',
      imgAlt: 'Photo of Mr. Tejpal Singh Batra',
      text: `A seasoned global leader with over 30 year of experience, have worked with great organizations like Thermax,
      MBT and AP Moller Maersk. Currently he is focusing on giving back to society via IGiftLife and teaches at various
      B-Schools as visiting faculty.`,
      socialMedia: {
        facebook: '',
        twitter: '',
        linkedin: '',
      },
    },
    {
      title: 'Ms. Naina Batra',
      subtitle: 'Co-founder and Secretary',
      imgSrc: 'assets/founders/igl trustees pic.png',
      imgAlt: 'Photo of Ms. Naina Batra',
      text: `As young and dynamic student, currently completing her MBA.
      Her passion for this cause is unparalleled and she continues to inspire many by her dedication to this cause.<br><br><br>`,
      socialMedia: {
        facebook: '',
        twitter: '',
        linkedin: '',
      },
    },
  ];
}
