import { Component } from '@angular/core';

@Component({
  selector: 'app-guidelines',
  templateUrl: './guidelines.component.html',
  styleUrls: ['./guidelines.component.scss']
})
export class GuidelinesComponent {

  guidelines = [
    {
      title: 'NOTP Operational Guidelines ',
      link: 'assets/guidelines/notp-operational-guidelines.pdf',
    },
  ];
  allocationCriteria = [
    {
      title: 'Updated Allocation criteria for Kidney (24.08.2016)',
      link: 'assets/guidelines/allocation-criteria/kidney.pdf',
    },
    {
      title: 'Updated Allocation Criteria for Liver (24.08.2016)',
      link: 'assets/guidelines/allocation-criteria/liver.pdf',
    },
    {
      title: 'Allocation Criteria for Heart, Lung and Heart-Lung',
      link: 'assets/guidelines/allocation-criteria/heart-lung.pdf',
    },
    {
      title: 'Allocation Criteria for Cornea',
      link: 'assets/guidelines/allocation-criteria/cornea.pdf',
    },
  ];
  sops = [
    {
      title: 'Management of Brain Dead Organ Donor in Intensive Care Unit (ICU)',
      link: 'assets/guidelines/sops/management-of-brain-dead-organ-donor-in-icu.pdf',
    },
    {
      title: 'Management of Brain Dead Organ Donor in Operation Theater (OT)',
      link: 'assets/guidelines/sops/management-of-brain-dead-organ-donor-in-ot.pdf',
    },
    {
      title: 'Donor Retrieval Surgery - Heart',
      link: 'assets/guidelines/sops/heart.pdf',
    },
    {
      title: 'Donor Retrieval Surgery - Liver',
      link: 'assets/guidelines/sops/liver.pdf',
    },
    {
      title: 'Donor Retrieval Surgery - Kidney',
      link: 'assets/guidelines/sops/kidney.pdf',
    },
    {
      title: 'Donor Retrieval Surgery - Cornea',
      link: 'assets/guidelines/sops/cornea.pdf',
    },
    {
      title: 'Reconstructive Transplant',
      link: 'assets/guidelines/sops/reconstructive-transplant.pdf',
    },
  ];
}
