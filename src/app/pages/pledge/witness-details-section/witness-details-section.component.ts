import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import emailMask from 'text-mask-addons/dist/emailMask';

@Component({
  selector: 'app-witness-details-section',
  templateUrl: './witness-details-section.component.html',
})
export class WitnessDetailsSectionComponent implements OnInit {
  formGroup: FormGroup;
  witnessDetails: any;

  phoneNumberMask = {
    mask: ['+', '9', '1', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
    guide: false,
  };
  emailMask = {
    mask: emailMask,
    guide: false,
  };

  constructor(public _fb: FormBuilder) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      fullName: ['', [Validators.required, Validators.maxLength(35)]],
      relationship: ['', [Validators.required, Validators.maxLength(35)]],
      address: ['', Validators.required],
      emailId: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required],
    });
  }

  onSubmit() {
    // var fullName = this.formGroup.get('fullName').value;
    // var relationship = this.formGroup.get('relationship').value;
    // var phoneNumber = this.formGroup.get('phoneNumber').value;
    // var emailId = this.formGroup.get('emailId').value;
    // var address = this.formGroup.get('address').value;

    // this.witnessDetails = {
    //   'fullName': fullName,
    //   'relationship': relationship,
    //   'phoneNumber': phoneNumber,
    //   'emailId': emailId,
    //   'address': address
    // };
  }

}
