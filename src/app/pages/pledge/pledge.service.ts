import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PledgeService {

  constructor(private http: HttpClient) { }

  submitPledge(allDetails) {
    return this.http.post('pledge/submitPledge', allDetails).pipe(map(
      (response: Response) => {}
    ));
  }
}
