import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-prepare-section',
  templateUrl: './prepare-section.component.html',
  styleUrls: ['./prepare-section.component.scss']
})
export class PrepareSectionComponent {
  @Output() sectionCompleted = new EventEmitter<boolean>();

  enableNextButton = false;
  checked = [false, false, false];

  setChecked(index) {
    this.checked[index] = !this.checked[index];
    this.enableNextButton = this.checked.reduce((previousValue, currentValue) => {
      return currentValue && previousValue;
    }, true);
    this.sectionCompleted.emit(this.enableNextButton);
  }
}
