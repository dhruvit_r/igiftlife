import { AwarenessComponent } from './../pages/about-organ-donation/awareness/awareness.component';
import { GuidelinesComponent } from './../pages/about-organ-donation/guidelines/guidelines.component';
import { OrganInformationComponent } from './../pages/about-organ-donation/organ-information/organ-information.component';
import { AboutOrganDonationComponent } from './../pages/about-organ-donation/about-organ-donation.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { PageNotFoundComponent } from '../pages/page-not-found/page-not-found.component';
import { FaqsComponent } from '../pages/faqs/faqs.component';
import { PledgeComponent } from '../pages/pledge/pledge.component';
import { ContactUsComponent } from '../pages/contact-us/contact-us.component';
import { OverviewComponent } from '../pages/about-organ-donation/overview/overview.component';
import { AboutUsComponent } from 'src/app/pages/about-us/about-us.component';
import { WhoWeAreComponent } from 'src/app/pages/about-us/who-we-are/who-we-are.component';
import { HistoryComponent } from 'src/app/pages/about-us/history/history.component';
import { AdvisorsComponent } from 'src/app/pages/about-us/advisors/advisors.component';
import { AwardsComponent } from 'src/app/pages/about-us/awards/awards.component';
import { CopyrightComponent } from 'src/app/pages/copyright/copyright.component' ;
import { DisclaimerComponent } from 'src/app/pages/disclaimer/disclaimer.component' ;
import { PrivacyComponent } from 'src/app/pages/privacy/privacy.component' ;
import { TermsComponent } from 'src/app/pages/terms/terms.component';
import { GalleryComponent } from 'src/app/pages/gallery/gallery.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'Home' },
  },
  {
    path: 'faqs',
    component: FaqsComponent,
    data: { title: 'FAQs' },
  },
  {
    path: 'pledge',
    component: PledgeComponent,
    data: { title: 'Pledge' },
  },
  {
    path: 'copyright',
    component: CopyrightComponent,
    data: { title: 'Copyright Policy' },
  },
  {
    path: 'terms',
    component: TermsComponent,
    data: { title: 'Terms and components' },
  },
  {
    path: 'disclaimer',
    component: DisclaimerComponent,
    data: { title: 'Disclaimer' },
  },
  {
    path: 'privacy',
    component: PrivacyComponent,
    data: { title: 'Privacy Policy' },
  },
  {
    path: 'gallery',
    component: GalleryComponent,
    data: { title: 'Gallery' },
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    data: {title: 'Contact Us'}
  },
  {
    path: 'about-organ-donation',
    component: AboutOrganDonationComponent,
    data: { title: 'About Organ Donation' },
    children: [
      {
        path: 'overview',
        component: OverviewComponent,
      },
      {
        path: 'organ-information',
        component: OrganInformationComponent,
      },
      {
        path: 'guidelines',
        component: GuidelinesComponent,
      },
      {
        path: 'awareness',
        component: AwarenessComponent,
      },
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    data: { title: 'About Us' },
    children: [
      {
        path: 'who-we-are',
        component: WhoWeAreComponent,
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
      {
        path: 'advisors',
        component: AdvisorsComponent,
      },
      {
        path: 'awards',
        component: AwardsComponent,
      },
      {
        path: '',
        redirectTo: 'who-we-are',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
