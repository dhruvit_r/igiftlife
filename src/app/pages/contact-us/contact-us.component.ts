import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as mailtoLink from 'mailto-link';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  formGroup: FormGroup;
  constructor(public _fb: FormBuilder) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      name: ['', [Validators.required, Validators.maxLength(35)]],
      subject: ['', [Validators.required]],
      message: ['', [Validators.required]],
    });
  }

  onSubmit() {
    let body = `From: ${this.formGroup.get('name').value}\n\n`;
    body += this.formGroup.get('message').value;
    const mailLink = mailtoLink({
      to: 'contact@igiftlife.com ',
      subject: this.formGroup.get('subject').value,
      body
    });
    document.location.href = mailLink;
  }

}
