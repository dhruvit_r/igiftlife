import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-id-verification-section',
  templateUrl: './id-verification-section.component.html',
})
export class IdVerificationSectionComponent implements OnInit {

  formGroup: FormGroup;
  selectedFile: File = null;
  fd: FormData;

  constructor(
    public _fb: FormBuilder,
    private http: HttpClient
    ) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      idCardType: ['', [Validators.required, Validators.maxLength(35)]],
      idCardNumber: ['', [Validators.required, Validators.maxLength(35)]],
      idCardPhoto: [null, Validators.required],
    });
  }

  onFileSelected(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.selectedFile = file;
    }
  }

  uploadPhoto() {
    this.fd = new FormData();
    this.fd.append('idCardNumber', this.formGroup.get('idCardNumber').value);
    this.fd.append('idCardPhoto', this.selectedFile);
    return this.http.post('pledge/idCardPhotoUpload', this.fd).pipe(
      map(
        (response: Response) => {},
        (error: Response) => {}
      )
    );
  }
}
