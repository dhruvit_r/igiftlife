import { Component } from '@angular/core';

@Component({
  selector: 'app-about-organ-donation',
  templateUrl: './about-organ-donation.component.html',
  styleUrls: ['./about-organ-donation.component.scss']
})
export class AboutOrganDonationComponent {
  tabs = [
    {
      title: 'Overview',
      url: 'overview',
    },
    {
      title: 'Organ Information',
      url: 'organ-information',
    },
    {
      title: 'Guidelines',
      url: 'guidelines',
    },
    {
      title: 'Awareness',
      url: 'awareness',
    },
  ];
}
