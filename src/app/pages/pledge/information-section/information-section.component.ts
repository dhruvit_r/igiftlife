import { Component, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-information-section',
  templateUrl: './information-section.component.html',
})
export class InformationSectionComponent {
  @Output() sectionCompleted = new EventEmitter<boolean>();

  setCompleted() {
    this.sectionCompleted.emit(true);
  }
}
