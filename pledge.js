const express = require('express');
const path = require('path');
var multer = require('multer');
var fs = require('fs');
const { createCanvas, loadImage } = require('canvas');
const canvas = createCanvas(1020, 500)
const ctx = canvas.getContext('2d')
PDFDocument = require('pdfkit')

var con = require('./dbconnection');
var email = require("emailjs");

const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//To check if a directory exists, if not create that directory
function ensureExists(path, mask, cb) {
  if (typeof mask == 'function') { // allow the `mask` parameter to be optional
    cb = mask;
    mask = 0777;
  }
  fs.mkdir(path, mask, function (err) {
    if (err) {
      if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
      else cb(err); // something else went wrong
    } else cb(null); // successfully created folder
  });
}

// set the directory for the uploads to be uploaded to  /* For localhost */
var DIR_img = path.join(__dirname, '/src/assets/uploads/id-card-photo/');

/* For server */
//var DIR_img = path.join(__dirname, '/dist/assets/uploads/id-card-photo/');

var storage = multer.diskStorage({
  //Giving the location of image to be saved
  destination: function (req, file, cb) {
    ensureExists(DIR_img, 0777, function (err) {

      if (err) throw err;// handle folder creation error
      else {
      }
    });
    cb(null, DIR_img)
  },
  //Renaming the image to be saved
  filename: function (req, file, cb) {
    //Getting the idCardNumber from req.body parameter

    cb(null, req.body.idCardNumber + path.extname(file.originalname))   //path.extname(file.originalname)
  }
})

/* For localhost */
var DIR_dc = path.join(__dirname, '/src/assets/uploads/donor-card-pdf/');

/* For server */
//var DIR_dc = path.join(__dirname, '/dist/assets/uploads/donor-card-pdf/');

//define the type of upload multer would be doing and pass in its destination, a single file with the name idCardPhoto
var upload = multer({ storage: storage }).single('idCardPhoto');

//Post request from id-verification-section.component.ts
app.post('/idCardPhotoUpload', function (req, res) {
  upload(req, res, function (err) {

    if (err) {
      return res.send(err.toString());
    }
    return res.json('Success');
  });
});

app.post('/submitPledge', function (req, res, next) {

  var fullName = req.body.fullName;
  var fathersName = req.body.fathersName;
  var gender = req.body.gender;
  var dateOfBirth = req.body.dateOfBirth;
  var bloodGroup = req.body.bloodGroup;
  var phoneNumber = req.body.phoneNumber;
  var emergencyContactNumber = req.body.emergencyContactNumber;
  var emailId = req.body.emailId;
  var address = req.body.address;
  var city = req.body.city;
  var state = req.body.state;
  var pinCode = req.body.pinCode;
  var idCardType = req.body.idCardType;
  var idCardNumber = req.body.idCardNumber;
  var idCardPhotoName = req.body.idCardPhotoName;
  var fullNameW1 = req.body.fullNameW1;
  var relationshipW1 = req.body.relationshipW1;
  var phoneNumberW1 = req.body.phoneNumberW1;
  var emailIdW1 = req.body.emailIdW1;
  var addressW1 = req.body.addressW1;
  var fullNameW2 = req.body.fullNameW2;
  var relationshipW2 = req.body.relationshipW2;
  var phoneNumberW2 = req.body.phoneNumberW2;
  var emailIdW2 = req.body.emailIdW2;
  var addressW2 = req.body.addressW2;
  var dataSharingConfirmation = req.body.dataSharingConfirmation;
  var organs = req.body.organs;
  var tissues = req.body.tissues;

  var imagePath = path.join(DIR_img, idCardNumber + path.extname(idCardPhotoName));

  var today = new Date();
  var dd = (today.getDate() < 10) ? '0' + today.getDate() : today.getDate();
  var mm = ((today.getMonth() + 1) < 10) ? '0' + today.getMonth() + 1 : today.getMonth() + 1;
  var yy = today.getFullYear();
  var currentDate = dd + '/' + mm + '/' + yy;
  var dbDate = yy + '-' + mm + '-' + dd;

  //storing values in donor_card, witness, organs and tissues tables
  var sql = "INSERT INTO donor_card VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); INSERT INTO witness (full_name,relationship,phone_number,email,address,id_card_number) VALUES (?,?,?,?,?,?); INSERT INTO witness (full_name,relationship,phone_number,email,address,id_card_number) VALUES (?,?,?,?,?,?); INSERT INTO organs (id_card_number,heart,lungs,kidneys,liver,pancreas,intestine,all_organs) VALUES (?,?,?,?,?,?,?,?); INSERT INTO tissues (id_card_number,corneas,skin,bones,heart_valves,blood_vessels,all_tissues) VALUES (?,?,?,?,?,?,?)";
  var values = [fullName, fathersName, gender, dateOfBirth, bloodGroup, phoneNumber, emergencyContactNumber, emailId, address, city, state, pinCode, idCardType, idCardNumber, imagePath, dbDate, dataSharingConfirmation, fullNameW1, relationshipW1, phoneNumberW1, emailIdW1, addressW1, idCardNumber, fullNameW2, relationshipW2, phoneNumberW2, emailIdW2, addressW2, idCardNumber, idCardNumber, organs['heart'], organs['lungs'], organs['kidneys'], organs['liver'], organs['pancreas'], organs['intestine'], organs['allOrgans'], idCardNumber, tissues['corneas'], tissues['skin'], tissues['bones'], tissues['heartValves'], tissues['bloodVessels'], tissues['allTissues']];

  con.query(sql, values, function (err, result) {
    if (err) return next(err);

    var bg = bloodGroup.split("");

    /*----- Comment this part for Notto Donor Card -----*/
    loadImage("organdonor1.jpg").then((image) => {
      ctx.drawImage(image, 0, 0, 500, 300)
      ctx.font = '15px Arial';
      ctx.fillText(fullName, 58, 140);
      ctx.fillStyle = "#ffffff";
      if (bg.length == 2) {
        ctx.fillText(bg[0], 108, 173);
        ctx.fillText(bg[1], 123, 173);

      } else if (bg.length == 3) {
        ctx.fillText(bg[0], 94, 173);
        ctx.fillText(bg[1], 108, 173);
        ctx.fillText(bg[2], 123, 173);
      }

      ctx.fillStyle = "#000000";
      ctx.fillText(phoneNumber, 123, 198);
      ctx.fillText(currentDate, 58, 226);

      ensureExists(DIR_dc, 0777, function (err) {

        if (err) throw err;// handle folder creation error
        else {
        }
      });

      fs.writeFile(path.join(DIR_dc, idCardNumber + '.jpg'), canvas.toBuffer(), (err) => {
        if (err) throw err;
      });
      //console.log('<img src="' + canvas.toDataURL() + '" />')
    })

    loadImage("organdonor2.jpg").then((image) => {
      ctx.drawImage(image, 520, 0, 500, 300)

      // ctx.beginPath();
      // ctx.moveTo(590, 135);
      // ctx.lineTo(600, 145);
      // ctx.stroke();

      fs.writeFile(path.join(DIR_dc, idCardNumber + '.jpg'), canvas.toBuffer(), (err) => {
        if (err) throw err;

        //Create a document
        doc = new PDFDocument

        doc.pipe(fs.createWriteStream(path.join(DIR_dc, idCardNumber + '.pdf')));
        doc.image(path.join(DIR_dc, idCardNumber + '.jpg'), 0, 0, { width: 520, height: 280 });
        doc.end()
        fs.unlink(path.join(DIR_dc, idCardNumber + '.jpg'), function (err) {
          if (err) throw err;
          // if no error, file has been deleted successfully
        });

        var server = email.server.connect({
          user: "igiftlifefoundation@gmail.com",
          password: "igiftlife@1234",
          host: "smtp.gmail.com",
          port: 465,
          ssl: true,
          timeout: 5000
        });

        var message = {
          text: "i hope this works",
          from: "IGiftLife <igiftlifefoundation@gmail.com>",
          to: "<" + emailId + ">",
          subject: "IGiftLife Pledge Donor Card",
          attachment:
            [
              { data: "<html>Your pledge for organ donation has been submitted successfully. Your donor card is attached with this mail, you can take a printout of it and keep it with you.</html>", alternative: true },
              { path: path.join(DIR_dc, idCardNumber + '.pdf'), type: "application/pdf", name: "Donor-Card.pdf" }
            ]
        };

        // send the message and get a callback with an error or details of the message that was sent
        server.send(message, function (err, message) { });
      });
    })
    /*----- Comment this part for Notto Donor Card -----*/
    res.json(result);
  });

})

module.exports = app;