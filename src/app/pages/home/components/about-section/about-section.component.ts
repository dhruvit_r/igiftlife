import { Component } from '@angular/core';

@Component({
  selector: 'app-about-section',
  templateUrl: './about-section.component.html',
  styleUrls: ['./about-section.component.scss']
})
export class AboutSectionComponent {
  infos = [
    {
      title: 'What is an organ?',
      content: 'An organ is a part of the body that performs a specific function: like your Heart, Lungs, Kidney, Liver etc.',
    },
    {
      title: 'What is tissue?',
      content: `Tissue means a group of cells performing a particular function in the human body. Examples would be bone, skin,
      cornea of the eye, heart valve, blood vessels, nerves and tendon etc.`,
    },
    {
      title: 'What is Organ Donation?',
      content: 'Organ Donation is the gift of an organ to a person with end stage organ disease and who needs a transplant.',
    },
  ];

  tables = [
    {
      title: 'What can be donated by a donor?',
      columns: [
        {
          key: 'status',
          title: 'Donor Status',
        },
        {
          key: 'whatCanBeDonated',
          title: 'What can be donated',
        }
      ],
      data: [
        { status: 'Alive', whatCanBeDonated: 'One Kidney, Part of liver, Bone Marrow, Part of Skin.' },
        { status: 'Death at Home', whatCanBeDonated: 'Cornea, Skin and Bones (in some cases)', },
        { status: 'Brain Death (at Hospital)', whatCanBeDonated: 'Kidney, Cornea, Liver, Skin, Heart, Bones, Pancreas and Lungs', },
        { status: 'Death (at Hospital)', whatCanBeDonated: 'Cornea, Skin and Bones (in some cases)', },
      ],
    },
    {
      title: 'For how long can organs be preserved?',
      pre: 'Healthy organs are transplanted from the body of the patient as soon as possible.',
      columns: [
        {
          key: 'organ',
          title: 'Organ',
        },
        {
          key: 'time',
          title: 'Retrieval to Transplant',
        }
      ],
      data: [
        { organ: 'Heart & Lungs', time: '4 to 6 hours' },
        { organ: 'Liver & Pancreas', time: '12 to 24 hours' },
        { organ: 'Kidneys', time: '24 to 48 hours' },
        { organ: 'Eyes & Skin ', time: 'Within 6 hours but can be stored for longer duration' },
      ],
      post: 'Green corridor helps a lot in this.',
    }
  ];

  columnKeys(object) {
    return object.map(column => column.key);
  }
}
