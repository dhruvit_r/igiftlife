import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-organ-tissue-section',
  templateUrl: './organ-tissue-section.component.html',
  styleUrls: ['./organ-tissue-section.component.scss']
})
export class OrganTissueSectionComponent implements OnInit {

  formGroup: FormGroup;
  checkedOrgans = {
    'heart': false,
    'lungs': false,
    'kidneys': false,
    'liver': false,
    'pancreas': false,
    'intestine': false,
    'allOrgans': false
  }
  checkedTissues = {
    'corneas': false,
    'skin': false,
    'bones': false,
    'heartValves': false,
    'bloodVessels': false,
    'allTissues': false
  }

  constructor(
    public _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      heart: false,
      lungs: false,
      kidneys: false,
      liver: false,
      pancreas: false,
      intestine: false,
      allOrgans: false,
      corneas: false,
      skin: false,
      bones: false,
      heartValves: false,
      bloodVessels: false,
      allTissues: false
    });
  }

  updateCheckedOrgans(option, event) {
    this.checkedOrgans[option] = event.checked;
  }

  updateCheckedTissues(option, event) {
    this.checkedTissues[option] = event.checked;
  }

}
