import { Component } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {

  tabs = [
    {
      title: 'Who We Are',
      url: 'who-we-are',
    },
    {
      title: 'Our History',
      url: 'history',
    },
    {
      title: 'Our Supporters',
      url: 'advisors',
    },
    {
      title: 'Awards',
      url: 'awards',
    },
  ];

}
