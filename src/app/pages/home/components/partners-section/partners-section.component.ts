import { Component } from '@angular/core';

@Component({
  selector: 'app-partners-section',
  templateUrl: './partners-section.component.html',
  styleUrls: ['./partners-section.component.scss']
})
export class PartnersSectionComponent {
  cards = [
    {
      imgSrc: 'assets/partners/capgemini.jpg',
      imgAlt: 'Capgemini Logo',
      text: `Capgemini`,
    },
    {
      imgSrc: 'assets/partners/concentrix.jpg',
      imgAlt: 'Concentrix Logo',
      text: `Concentrix`,
    },
    {
      imgSrc: 'assets/partners/ewge.png',
      imgAlt: 'EWGE Logo',
      text: `EWGE`,
    },
    {
      imgSrc: 'assets/partners/flame.jpg',
      imgAlt: 'Flame University Logo',
      text: `Flame University`,
    },
    {
      imgSrc: 'assets/partners/idias.jpg',
      imgAlt: 'IDeaS - A SAS Company Logo',
      text: `IDeaS - A SAS Company`,
    },
    {
      imgSrc: 'assets/partners/iiebm.jpg',
      imgAlt: 'Indian Institute of Education and Business Management Logo',
      text: `IIEBM, Pune`,
    },
    {
      imgSrc: 'assets/partners/iim-indore.jpg',
      imgAlt: 'Indian Institute of Management Indore Logo',
      text: `IIM, Indore`,
    },
    {
      imgSrc: 'assets/partners/infosys.jpg',
      imgAlt: 'Infosys Logo',
      text: `Infosys`,
    },
    {
      imgSrc: 'assets/partners/isbm.jpg',
      imgAlt: 'ISB&M School Of Technology Logo',
      text: `ISB&M School Of Technology`,
    },
    {
      imgSrc: 'assets/partners/mahindra.jpg',
      imgAlt: 'Mahindra Logo',
      text: `Mahindra`,
    },
    {
      imgSrc: 'assets/partners/newgrowth.png',
      imgAlt: 'NewGrowth Logo',
      text: `NewGrowth`,
    },
    {
      imgSrc: 'assets/partners/nhrdn.jpg',
      imgAlt: 'National HRD Network Logo',
      text: `National HRD Network`,
    },
    {
      imgSrc: 'assets/partners/people-strong.png',
      imgAlt: 'PeopleStrong Logo',
      text: `PeopleStrong`,
    },
    {
      imgSrc: 'assets/partners/pibm.jpg',
      imgAlt: 'Pune Institute of Business Management Logo',
      text: `PIBM, Pune`,
    },
    {
      imgSrc: 'assets/partners/sims.jpg',
      imgAlt: 'Symbiosis Institute of Management Studies Logo',
      text: `SIMS, Pune`,
    },
    {
      imgSrc: 'assets/partners/sri-balaji-society.jpg',
      imgAlt: 'Sri Balaji Society Logo',
      text: `Sri Balaji Society`,
    },
    {
      imgSrc: 'assets/partners/tata.jpg',
      imgAlt: 'Tata Group Logo',
      text: `Tata Group`,
    },
    {
      imgSrc: 'assets/partners/tieto.jpg',
      imgAlt: 'Tieto Logo',
      text: `Tieto`,
    },
    {
      imgSrc: 'assets/partners/voltas.jpg',
      imgAlt: 'Voltas Logo',
      text: `Voltas`,
    },
    {
      imgSrc: 'assets/partners/wipro.jpg',
      imgAlt: 'Wipro Logo',
      text: `Wipro`,
    },
    {
      imgSrc: 'assets/partners/wordsmaya.jpg',
      imgAlt: 'WordsMaya Logo',
      text: `WordsMaya`,
    },
    {
      imgSrc: 'assets/partners/xento.jpg',
      imgAlt: 'Xento Systems Logo',
      text: `Xento Systems`,
    },
  ];
}
