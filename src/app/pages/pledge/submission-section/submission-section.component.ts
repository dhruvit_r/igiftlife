import { Component, Input } from '@angular/core';
import { PledgeComponent } from '../pledge.component';


@Component({
  selector: 'app-submission-section',
  templateUrl: './submission-section.component.html',
  styleUrls: ['./submission-section.component.scss']
})
export class SubmissionSectionComponent {

  _canSubmit = false;
  ageConfirmation = false;
  legalConfirmation = false;
  dataSharingConfirmation = false;
  buttonDisabled = false;
  submit = false;

  constructor(
    private pledgeComponent: PledgeComponent
  ) { }

  @Input('canSubmit')
  get canSubmit() {
    return this._canSubmit;
  }
  set canSubmit(v: boolean) {
    this._canSubmit = v;
    this.updateCanSubmit();
  }

  updateCanSubmit() {
    this.buttonDisabled = !(this._canSubmit && this.ageConfirmation && this.legalConfirmation);
  }

  submission() {
    this.submit = true;
    this.pledgeComponent.submitPledge(this.submit, this.dataSharingConfirmation);
  }
}
