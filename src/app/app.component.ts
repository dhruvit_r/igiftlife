import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  socialMediaItems = [
    {
      url: 'https://www.facebook.com/igiftlife',
      icon: 'facebook',
      title: 'Facebook',
    },
    {
      url: 'https://twitter.com/igiftlife',
      icon: 'twitter',
      title: 'Twitter',
    },
    {
      url: 'https://www.linkedin.com/company/igiftlife/',
      icon: 'linkedin',
      title: 'LinkdIn',
    },
    {
      url: 'https://www.youtube.com/channel/UCPdI8ehOTCNEks1M0xsAEdA',
      icon: 'youtube',
      title: 'YouTube',
    },
    {
      url: 'https://www.instagram.com/igiftlife/',
      icon: 'instagram',
      title: 'Instagram',
    },
  ];

  constructor(matIconRegistery: MatIconRegistry, sanitizer: DomSanitizer, public router: Router) {

    matIconRegistery.addSvgIcon('facebook', sanitizer.bypassSecurityTrustResourceUrl('assets/social-media-icons/facebook.svg'));
    matIconRegistery.addSvgIcon('twitter', sanitizer.bypassSecurityTrustResourceUrl('assets/social-media-icons/twitter.svg'));
    matIconRegistery.addSvgIcon('linkedin', sanitizer.bypassSecurityTrustResourceUrl('assets/social-media-icons/linkedin.svg'));
    matIconRegistery.addSvgIcon('youtube', sanitizer.bypassSecurityTrustResourceUrl('assets/social-media-icons/youtube.svg'));
    matIconRegistery.addSvgIcon('instagram', sanitizer.bypassSecurityTrustResourceUrl('assets/social-media-icons/instagram.svg'));
  }
}
