import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {

  pictures = [
    {
      title: 'Massive event of engaging close to 1000 students' ,
      img: 'assets/gallery/1000students.jpg'
    },
    {
      title: 'ARYAOMNITALK WIRELESS' ,
      img: 'assets/gallery/ARYAOMNITALK-WIRELESS2.jpg'
    },
    {
      title: 'Eon IT Park.' ,
      img: 'assets/gallery/eoinitpark.jpg'
    },
    {
      title: 'Infosys Jaipur And IBM India DLF Silokhera, Gurgaon And IBM' ,
      img: 'assets/gallery/infisys jaipur.jpg'
    },
    {
      title: 'Session With Colleagues From Avendus Capital.' ,
      img: 'assets/gallery/mumbai.jpg'
    },
    {
      title: 'Piaggio, Pune' ,
      img: 'assets/gallery/piago.jpg'
    },
    {
      title: 'Pune Airport' ,
      img: 'assets/gallery/puneairport4.jpg'
    },
    {
      title: 'Symbiosis Institute Of Technology' ,
      img: 'assets/gallery/students.jpg'
    },
    {
      title: 'Symbiosis International University - SIU.' ,
      img: 'assets/gallery/sui.jpg'
    },
    {
      title: 'Symbiosis International University - SIU.' ,
      img: 'assets/gallery/Symbiosis.jpg'
    },
    {
      title: 'SITM - Symbiosis Institute Of Telecom Management' ,
      img: 'assets/gallery/event3img1.jpg'
    },
    {
      title: 'Universal Business School' ,
      img: 'assets/gallery/event2imag4.jpg'
    },
    {
      title: 'IBM Pune Hinjewadi Office.' ,
      img: 'assets/gallery/eventimage1.jpg'
    },
    {
      title: 'Symbiosis Institute Of Management Studies,Pune' ,
      img: 'assets/gallery/symb1.jpg'
    },
    {
      title: 'NeoGrowth Credit Pvt Ltd, Mumbai.' ,
      img: 'assets/gallery/newsession1-min.jpg'
    },
    {
      title: 'IBM Hyderabad' ,
      img: 'assets/gallery/ibm1.jpg'
    },
    {
      title: 'NHRDN, Pune' ,
      img: 'assets/gallery/n2.jpg'
    },
    {
      title: 'Tieto, Pune' ,
      img: 'assets/gallery/t1.jpg'
    },
    {
      title: 'Infosys And IBM, Chennai' ,
      img: 'assets/gallery/infosys1.jpg'
    },
    {
      title: 'SOLVERMINDS And IBM, Chennai' ,
      img: 'assets/gallery/32984763_759861330870094_4839529455045050368_o-1.jpg'
    },
    {
      title: 'Voltas Limited, Jamshedpur' ,
      img: 'assets/gallery/voltas.jpg'
    },
    {
      title: 'Gluck International' ,
      img: 'assets/gallery/voltas.jpg'
    },
    {
      title: 'IBM, Pune' ,
      img: 'assets/gallery/ibmpune.jpg'
    },
    {
      title: 'Voltas Limited, Jameshedpur.' ,
      img: 'assets/gallery/voltasjam.jpg'
    },
    {
      title: 'Symbiosis, Pune' ,
      img: 'assets/gallery/sybmpun.jpg'
    },
    {
      title: 'Ahmednagar' ,
      img: 'assets/gallery/Ahmednagar-event-2.jpg'
    },
    {
      title: 'T G Terminals, Panvel,New Mumbai, Khalapur' ,
      img: 'assets/gallery/tgterminal.jpg'
    },
    {
      title: 'Mumbai, Delhi, Calcutta, Nariman Point, Bangalore' ,
      img: 'assets/gallery/mumbai-kolkata-bangalore-events-1-1.jpg'
    },
    {
      title: 'KPCL Hadapsar And Sasawad.' ,
      img: 'assets/gallery/kpcl.jpg'
    },
    {
      title: 'Kirloskar Pneumatic Company Limited, Pune' ,
      img: 'assets/gallery/kirloskar.jpg'
    },
    {
      title: 'Doctors Event, Delhi' ,
      img: 'assets/gallery/doc.jpg'
    },
    {
      title: 'Symbiosis Institute Of Management Studies' ,
      img: 'assets/gallery/sibm.jpg'
    },
    {
      title: 'IDeaS Revenue Solutions, Pune' ,
      img: 'assets/gallery/ideas.jpg'
    },
    {
      title: 'Infosys, Pune' ,
      img: 'assets/gallery/infosysp.jpg'
    },
    {
      title: 'Mothers Day' ,
      img: 'assets/gallery/m5.jpg'
    },
    {
      title: 'C3IT Software Solutions Pvt. Ltd, Pune' ,
      img: 'assets/gallery/c3it.jpg'
    },
    {
      title: 'Xento Systems, Pune' ,
      img: 'assets/gallery/xento1.jpg'
    },
    {
      title: 'IBM Concentrix Daksh Services Pvt Ltd, Mumbai' ,
      img: 'assets/gallery/mumbai1.jpg'
    },
    {
      title: 'Barclays Technology Centre, Pune' ,
      img: 'assets/gallery/brac1.jpg'
    },
    {
      title: 'Concentrix, Pune' ,
      img: 'assets/gallery/conc1.jpg'
    },
    {
      title: 'Capgemini, Pune' ,
      img: 'assets/gallery/capgemini.jpg'
    },
    {
      title: 'FLAME University, Pune' ,
      img: 'assets/gallery/flame.jpg'
    },
    {
      title: 'Tata Management Training Centre, Pune' ,
      img: 'assets/gallery/tatamag.jpg'
    },
    {
      title: 'Peoplestrong' ,
      img: 'assets/gallery/people.jpg'
    },
    {
      title: 'Cholamandalam Investment, Chennai' ,
      img: 'assets/gallery/chola.jpg'
    },
    {
      title: 'WordsMaya, Facebook Session' ,
      img: 'assets/gallery/wordsmaya.jpg'
    },
    {
      title: 'PIBM, Pune' ,
      img: 'assets/gallery/pibm.jpg'
    },
    {
      title: 'IIEBM, PIBM, ISB&M, SIMS And SBS, Pune' ,
      img: 'assets/gallery/bschool3.jpg'
    },
    {
      title: 'Symbiosis Institute Of Management Studies, Pune.' ,
      img: 'assets/gallery/symboisys2.jpg'
    },
    {
      title: 'Newgen Softwares Technology Ltd, Noida' ,
      img: 'assets/gallery/newgen1.jpg'
    },
    {
      title: 'Tech Mahindra, Pune' ,
      img: 'assets/gallery/tech2.jpg'
    },
    {
      title: 'Tieto, Pune' ,
      img: 'assets/gallery/tiyato1.jpg'
    },
    {
      title: 'Tech Mahindra' ,
      img: 'assets/gallery/techm.jpg'
    },
    {
      title: 'Clarion Technologies' ,
      img: 'assets/gallery/clarion.jpg'
    },

  ];
  constructor() { }


}
