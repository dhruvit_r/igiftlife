import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import emailMask from 'text-mask-addons/dist/emailMask';
import { PledgeService } from '../pledge.service';

@Component({
  selector: 'app-personal-details-section',
  templateUrl: './personal-details-section.component.html',
})
export class PersonalDetailsSectionComponent implements OnInit {
  formGroup: FormGroup;
  allDetails: any;

  phoneNumberMask = {
    mask: ['+', '9', '1', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/],
    guide: false,
  };
  emailMask = {
    mask: emailMask,
    guide: false,
  };

  constructor(
    public _fb: FormBuilder,
    private pledgeService: PledgeService
    ) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      fullName: ['', [Validators.required, Validators.maxLength(35)]],
      fathersName: ['', [Validators.required, Validators.maxLength(35)]],
      dateOfBirth: ['', Validators.required],
      gender: ['', Validators.required],
      bloodGroup: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      emergencyContactNumber: ['', Validators.required],
      emailId: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      pinCode: ['', Validators.required],
    });
  }

  onSubmit() {
    // var fullName = this.formGroup.get('fullName').value;
    // var fathersName = this.formGroup.get('fathersName').value;
    // var gender = this.formGroup.get('gender').value;
    // var bloodGroup = this.formGroup.get('bloodGroup').value;
    // var phoneNumber = this.formGroup.get('phoneNumber').value;
    // var emergencyContactNumber = this.formGroup.get('emergencyContactNumber').value;
    // var emailId = this.formGroup.get('emailId').value;
    // var address = this.formGroup.get('address').value;
    // var city = this.formGroup.get('city').value;
    // var state = this.formGroup.get('state').value;
    // var pinCode = this.formGroup.get('pinCode').value;

    // var date = (this.formGroup.get('dateOfBirth').value._i.date < 10) ? "0" + this.formGroup.get('dateOfBirth').value._i.date
    // : this.formGroup.get('dateOfBirth').value._i.date;
    // var month = (this.formGroup.get('dateOfBirth').value._i.month < 10) ? "0" + this.formGroup.get('dateOfBirth').value._i.month
    // : this.formGroup.get('dateOfBirth').value._i.month;
    // var dateOfBirth = this.formGroup.get('dateOfBirth').value._i.year + "-" + month + "-" + date;

    // this.allDetails = {
    //   'fullName': fullName,
    //   'fathersName': fathersName,
    //   'gender': gender,
    //   'dateOfBirth': dateOfBirth,
    //   'bloodGroup': bloodGroup,
    //   'phoneNumber': phoneNumber,
    //   'emergencyContactNumber': emergencyContactNumber,
    //   'emailId': emailId,
    //   'address': address,
    //   'city': city,
    //   'state': state,
    //   'pinCode': pinCode
    // };
  }
}
