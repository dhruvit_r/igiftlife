import { FaqService } from './../../providers/faq.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  infos = [];

  constructor(public faqService: FaqService) {}

  ngOnInit() {
    this.faqService.getAll().subscribe(faqs => {
      this.infos = faqs;
    });
  }
}
