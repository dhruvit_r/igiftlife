import { Component, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-carousel-section',
  templateUrl: './carousel-section.component.html',
  styleUrls: ['./carousel-section.component.scss']
})
export class CarouselSectionComponent implements AfterViewInit {
  images: String[] = [
    'assets/carousel/01.png',
    'assets/carousel/02.jpg',
    'assets/carousel/03.png',
    'assets/carousel/04.png',
    'assets/carousel/05.png',
    'assets/carousel/06.jpg',
  ];
  carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
    slide: 3,
    speed: 500,
    point: {
      visible: true
    },
    interval: {
      timing: 3000,
      initialDelay: 3000
    },
    loop: true,
    load: 2,
    touch: true,
    easing: 'ease'
  };

  constructor(public cdr: ChangeDetectorRef) { }

  ngAfterViewInit() {
    // this is used to renderer the carousel changes
    this.cdr.detectChanges();
  }

}
