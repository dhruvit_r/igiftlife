const express = require('express');
const http = require('http');
const path = require('path');
var compression = require('compression')

const app = express();

app.use(compression())

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist', 'IGiftLife')));
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'IGiftLife', 'index.html'));
});

const port = process.env.PORT || '3001';
app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => console.log('Running'));

var con = require('./dbconnection');

var pledge = require('./pledge');
app.use('/pledge', pledge);