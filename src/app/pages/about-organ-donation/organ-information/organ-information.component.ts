import { Component } from '@angular/core';

@Component({
  selector: 'app-organ-information',
  templateUrl: './organ-information.component.html',
  styleUrls: ['./organ-information.component.scss']
})
export class OrganInformationComponent {

  table1Columns = [ 'status', 'whatCanBeDonated' ];
  table1Data = [
    { status: 'Alive', whatCanBeDonated: 'Kidney, Part of liver, Bone Marrow, Part of Skin', },
    { status: 'Death At Home', whatCanBeDonated: 'Cornea, Skin and Bones (in some cases)', },
    { status: 'At Hospital', whatCanBeDonated: '', },
    { status: 'Brain Death', whatCanBeDonated: 'Kidney, Cornea, Liver, Skin, Heart, Bones, Pancreas and Lungs', },
    { status: 'Others', whatCanBeDonated: 'Cornea, Skin', },
  ];

  table2Columns = ['organ', 'timePeriod' ];
  table2Data = [
    { organ: 'Heart and Lungs', timePeriod: '4 to 6 hours' },
    { organ: 'Liver and Pancreas', timePeriod: '12 to 24 hours' },
    { organ: 'Kidneys', timePeriod: '24 to 48 hours' },
    { organ: 'Eyes and Skin', timePeriod: '6 hours but can be stored for longer duration' },
  ];

}
