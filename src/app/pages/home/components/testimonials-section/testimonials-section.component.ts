import { Component } from '@angular/core';

@Component({
  selector: 'app-testimonials-section',
  templateUrl: './testimonials-section.component.html',
  styleUrls: ['./testimonials-section.component.scss']
})
export class TestimonialsSectionComponent {
  cards = [
    {
      title: 'Dr. Neeru Bhagat',
      subtitle: 'Professor, SIT, Pune',
      imgSrc: 'assets/portraits/neeru.jpg',
      imgAlt: 'Photo of Neeru Bhagat',
      text: `The NGO, IGiftLife, is doing a really wonderful work which directly benefits the society in a big way on may fronts.
       I hope these efforts do lead to dispelling the doubts and myths and bring about a massive change around us and
       many lives are saved.`,
    }
  ];
}
