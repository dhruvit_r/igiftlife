import { MatSidenav } from '@angular/material';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent {

  @Input() sidenav: MatSidenav;

  navigationButtons = [
    {
      icon: 'home',
      title: 'Home',
      route: '/home',
    },
    {
      icon: 'help',
      title: 'Frequently Asked Questions',
      route: '/faqs',
    },
    {
      icon: 'assignment_turned_in',
      title: 'Register for Organ Donation',
      route: '/pledge',
    },
    {
      icon: 'contact_mail',
      title: 'Contact Us',
      route: '/contact-us'
    },
    {
      icon: 'accessibility_new',
      title: 'About Us',
      route: '/about-us'
    },
    {
      icon: 'photos',
      title: 'Gallery',
      route: '/gallery'
    },
    {
      icon: 'info',
      title: 'About Organ Donation',
      route: '/about-organ-donation',
    }
  ];
  legalItems = [
    {
      title: 'Transplantation of Human Organs and Tissues Rules, 2014',
      url: 'assets/legal/THOA-Rules-2014.pdf',
    },
    {
      title: 'THOA amendment 2011',
      url: 'assets/legal/THOA-amendment-2011.pdf',
    },
    {
      title: 'Transplantation Of Human Organs(Amendment) Rules,2008',
      url: 'assets/legal/Transplantation-Of-Human-Organs-(Amendment)-Rules-2008.pdf',
    },
    {
      title: 'THO Rules, 1995 (Original Rules)',
      url: 'assets/legal/THO-Rules-1995-(Original-Rules).pdf',
    },
    {
      title: 'THOA 1994',
      url: 'assets/legal/THOA-ACT-1994.pdf',
    },
  ];

  constructor(public router: Router) {}

}
