// @angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexModule } from '@angular/flex-layout/flex';
import { GridModule } from '@angular/flex-layout/grid';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

// 3rd Party Modules
import { NguCarouselModule } from '@ngu/carousel';
import { TextMaskModule } from 'angular2-text-mask';

// App Modules
import { RoutingModule } from './modules/routing.module';
import { MaterialModule } from './modules/material.module';

// App and related components
import { AppComponent } from './app.component';
import { SidenavListComponent } from './components/sidenav-list/sidenav-list.component';
// Home page and related components
import { HomeComponent } from './pages/home/home.component';
import { CarouselSectionComponent } from './pages/home/components/carousel-section/carousel-section.component';
import { AboutSectionComponent } from './pages/home/components/about-section/about-section.component';
import { FoundersSectionComponent } from './pages/home/components/founders-section/founders-section.component';
import { PartnersSectionComponent } from './pages/home/components/partners-section/partners-section.component';
import { TestimonialsSectionComponent } from './pages/home/components/testimonials-section/testimonials-section.component';
// FAQ page component
import { FaqsComponent } from './pages/faqs/faqs.component';
// 404 Page component
import { PageNotFoundComponent } from './pages//page-not-found/page-not-found.component';
// Pledge Component
import { PledgeComponent } from './pages/pledge/pledge.component';
import { PrepareSectionComponent } from './pages/pledge/prepare-section/prepare-section.component';
import { InformationSectionComponent } from './pages/pledge/information-section/information-section.component';
import { PersonalDetailsSectionComponent } from './pages/pledge/personal-details-section/personal-details-section.component';
import { IdVerificationSectionComponent } from './pages/pledge/id-verification-section/id-verification-section.component';
import { WitnessDetailsSectionComponent } from './pages/pledge/witness-details-section/witness-details-section.component';
import { SubmissionSectionComponent } from './pages/pledge/submission-section/submission-section.component';

// Pledge Service
import { PledgeService } from './pages/pledge/pledge.service';

// Pipes
import { NewlinePipe } from './pipes/newline.pipe';
import { AboutOrganDonationComponent } from './pages/about-organ-donation/about-organ-donation.component';
import { OverviewComponent } from './pages/about-organ-donation/overview/overview.component';
import { OrganInformationComponent } from './pages/about-organ-donation/organ-information/organ-information.component';
import { GuidelinesComponent } from './pages/about-organ-donation/guidelines/guidelines.component';
import { AwarenessComponent } from './pages/about-organ-donation/awareness/awareness.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { WhoWeAreComponent } from './pages/about-us/who-we-are/who-we-are.component';
import { HistoryComponent } from './pages/about-us/history/history.component';
import { AdvisorsComponent } from './pages/about-us/advisors/advisors.component';
import { AwardsComponent } from './pages/about-us/awards/awards.component';
import { FooterComponent } from './components/footer/footer.component';
import { DisclaimerComponent } from './pages/disclaimer/disclaimer.component';
import { CopyrightComponent } from './pages/copyright/copyright.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { TermsComponent } from './pages/terms/terms.component';
import { GalleryComponent } from './pages/gallery/gallery.component';

import { OrganTissueSectionComponent } from './pages/pledge/organ-tissue-section/organ-tissue-section.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarouselSectionComponent,
    AboutSectionComponent,
    FoundersSectionComponent,
    PartnersSectionComponent,
    TestimonialsSectionComponent,
    FaqsComponent,
    PageNotFoundComponent,
    PledgeComponent,
    PrepareSectionComponent,
    InformationSectionComponent,
    PersonalDetailsSectionComponent,
    IdVerificationSectionComponent,
    WitnessDetailsSectionComponent,
    SubmissionSectionComponent,
    NewlinePipe,
    SidenavListComponent,
    AboutOrganDonationComponent,
    OverviewComponent,
    OrganInformationComponent,
    GuidelinesComponent,
    AwarenessComponent,
    ContactUsComponent,
    AboutUsComponent,
    WhoWeAreComponent,
    HistoryComponent,
    AdvisorsComponent,
    AwardsComponent,
    FooterComponent,
    DisclaimerComponent,
    CopyrightComponent,
    PrivacyComponent,
    TermsComponent,
    GalleryComponent,
    OrganTissueSectionComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    FlexModule,
    GridModule,
    RoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    NguCarouselModule,
    TextMaskModule,
    ToastrModule.forRoot({timeOut: 3000})
  ],
  providers: [
    PledgeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
