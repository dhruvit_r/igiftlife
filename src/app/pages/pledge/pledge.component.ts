import { Component, ViewChild, AfterViewInit, ViewChildren } from '@angular/core';
import lodashFill from 'lodash/fill';
import lodashEvery from 'lodash/every';
import { PersonalDetailsSectionComponent } from './personal-details-section/personal-details-section.component';
import { IdVerificationSectionComponent } from './id-verification-section/id-verification-section.component';
import { WitnessDetailsSectionComponent } from './witness-details-section/witness-details-section.component';
import { PledgeService } from './pledge.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { OrganTissueSectionComponent } from './organ-tissue-section/organ-tissue-section.component';

@Component({
  selector: 'app-pledge',
  templateUrl: './pledge.component.html',
  styleUrls: ['./pledge.component.scss']
})
export class PledgeComponent implements AfterViewInit {

  constructor(
    private pledgeService: PledgeService,
    private toastrService: ToastrService,
    private http: HttpClient
  ) { }

  @ViewChild(PersonalDetailsSectionComponent) personalDetailsSectionComponent: PersonalDetailsSectionComponent;
  @ViewChild(IdVerificationSectionComponent) idVerificationSectionComponent: IdVerificationSectionComponent;
  @ViewChild(OrganTissueSectionComponent) organTissueSectionComponent: OrganTissueSectionComponent;
  @ViewChild('witnessOne') witnessDetailsSectionOne: WitnessDetailsSectionComponent;
  @ViewChild('witnessTwo') witnessDetailsSectionTwo: WitnessDetailsSectionComponent;

  completionStatus: Array<boolean> = lodashFill(Array(7), false);
  canSubmit = false;

  ngAfterViewInit() {

    this.personalDetailsSectionComponent.formGroup.valueChanges.subscribe(() => {
      this.setCompletionStatus(2, this.personalDetailsSectionComponent.formGroup.valid);
    });
    this.idVerificationSectionComponent.formGroup.valueChanges.subscribe(() => {
      this.setCompletionStatus(3, this.idVerificationSectionComponent.formGroup.valid);
    });
    this.organTissueSectionComponent.formGroup.valueChanges.subscribe(() => {
      this.setCompletionStatus(4, this.organTissueSectionComponent.formGroup.valid);
    });
    this.witnessDetailsSectionOne.formGroup.valueChanges.subscribe(() => {
      this.setCompletionStatus(5, this.witnessDetailsSectionOne.formGroup.valid);
    });
    this.witnessDetailsSectionTwo.formGroup.valueChanges.subscribe(() => {
      this.setCompletionStatus(6, this.witnessDetailsSectionTwo.formGroup.valid);
    });
  }

  setCompletionStatus(index, event) {
    this.completionStatus[index] = event;
    this.canSubmit = lodashEvery(this.completionStatus);
  }

  stepChanged(event, stepper) {
    stepper.selected.interacted = false;
  }

  submitPledge(submit: boolean, dataSharingConfirmation: boolean) {
    if (submit === true) {
      const personalDetailsFormGroup = this.personalDetailsSectionComponent.formGroup;
      const idVerificationFormGroup = this.idVerificationSectionComponent.formGroup;
      const witnessOneFormGroup = this.witnessDetailsSectionOne.formGroup;
      const witnessTwoFormGroup = this.witnessDetailsSectionTwo.formGroup;

      // Personal Details, Formatting Date Of Birth
      const dob = personalDetailsFormGroup.get('dateOfBirth').value._i;
      const date = ((dob.date < 10) ? '0' : '') + dob._i.date;
      const month = (dob._i.month < 10) ? '0' + dob._i.month : dob._i.month;
      const dateOfBirth = dob._i.year + '-' + month + '-' + date;

      const allDetails = {
        // Personal Details
        'fullName': personalDetailsFormGroup.get('fullName').value,
        'fathersName': personalDetailsFormGroup.get('fathersName').value,
        'gender': personalDetailsFormGroup.get('gender').value,
        'dateOfBirth': dateOfBirth,
        'bloodGroup': personalDetailsFormGroup.get('bloodGroup').value,
        'phoneNumber': personalDetailsFormGroup.get('phoneNumber').value,
        'emergencyContactNumber': personalDetailsFormGroup.get('emergencyContactNumber').value,
        'emailId': personalDetailsFormGroup.get('emailId').value,
        'address': personalDetailsFormGroup.get('address').value,
        'city': personalDetailsFormGroup.get('city').value,
        'state': personalDetailsFormGroup.get('state').value,
        'pinCode': personalDetailsFormGroup.get('pinCode').value,

        // ID Verification Details
        'idCardType': idVerificationFormGroup.get('idCardType').value,
        'idCardNumber': idVerificationFormGroup.get('idCardNumber').value,
        'idCardPhotoName': this.idVerificationSectionComponent.selectedFile.name,

        //Organs and Tissues
        'organs': this.organTissueSectionComponent.checkedOrgans,
        'tissues': this.organTissueSectionComponent.checkedTissues,

        //Witness One Details
        'fullNameW1': this.witnessDetailsSectionOne.formGroup.get('fullName').value,
        'relationshipW1': this.witnessDetailsSectionOne.formGroup.get('relationship').value,
        'phoneNumberW1': this.witnessDetailsSectionOne.formGroup.get('phoneNumber').value,
        'emailIdW1': this.witnessDetailsSectionOne.formGroup.get('emailId').value,
        'addressW1': this.witnessDetailsSectionOne.formGroup.get('address').value,

        // Witness Two Details
        'fullNameW2': witnessTwoFormGroup.get('fullName').value,
        'relationshipW2': witnessTwoFormGroup.get('relationship').value,
        'phoneNumberW2': witnessTwoFormGroup.get('phoneNumber').value,
        'emailIdW2': witnessTwoFormGroup.get('emailId').value,
        'addressW2': witnessTwoFormGroup.get('address').value,

        // Data Sharing Confirmation
        'dataSharingConfirmation': dataSharingConfirmation
      };
      this.idVerificationSectionComponent.uploadPhoto().subscribe(
        data => {
          this.pledgeService.submitPledge(allDetails).subscribe(
            res => {
              this.toastrService.success('Pledge submitted successfully! Donor Card will be mailed to you', 'Success!');

              /*----- For NOTTO Donor Card -----*/
              //this.toastrService.success('Pledge submitted successfully! Donor Card will be mailed to you within a week', 'Success!');
              /*----- For NOTTO Donor Card -----*/

              setTimeout("location.reload(true);", 3000);

            }, error => {
              this.toastrService.error('Something happened while submitting the pledge', 'Error');
            }
          );
        }, error => {
          this.toastrService.error('Some problem while uploading the ID Card Image', 'Error');
        }
      );
    }
  }
}
